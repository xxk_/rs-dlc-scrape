import lxml.etree as ET

class TableWriter():

    dlc_lists = []
    tree = None
    out_file = None

    def create_tree(self):
        self.tree = ET.Element("DLC_ENTRIES")
        for dlc_list in self.dlc_lists:
            for DLC in dlc_list:
                entry = ET.SubElement(self.tree, "DLC")
                entry.set("NAME", DLC.name)
                entry.set("PC_LINK", DLC.pc_url)
                entry.set("MAC_LINK", DLC.mac_url)
                entry.set("TORRENT", DLC.torrent_url)

    def write(self):
        self.out_file.write((ET.tostring(self.tree, pretty_print=True)))

    def __init__(self, dlc_lists):
        self.dlc_lists = dlc_lists
        self.out_file = open("DumpedLinks.xml", "wb+")
        self.create_tree()
        self.write()
        self.out_file.close()