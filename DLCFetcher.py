import os

from browser import TorBrowser
from selenium import webdriver
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
from selenium.webdriver.firefox.firefox_profile import FirefoxProfile

from dlc import DLC

class DLCFetcher(TorBrowser):

    def collect_thread_urls(self):
        if not self.dlc_posts:
            print("dlcPosts is empty!")
            return

        for post in self.dlc_posts:
            self.dlc_post_urls.append(post.find_element_by_tag_name("a").get_attribute("href"))

    def debug_write_info_to_file(self):
        if len(self.dlc_post_urls) == 0:
                print("dlc_post_urls were not collected")
                return
        outputFile = open("output\\" + str(self.page_number) + "_output.txt", "w+")
        for postURL in self.dlc_post_urls:
            outputFile.write(postURL + "\n")

    def connect_to_thread(self):
        for url in self.dlc_post_urls:
            self.driver.get(url)
            print(self.driver.title[12:])
            name = self.driver.title[12:]
            PClink = self.driver.find_element_by_link_text("PC").get_attribute("href")
            MAClink =  self.driver.find_element_by_link_text("MAC").get_attribute("href")
            TORRENTlink = self.driver.find_element_by_link_text("Mediafire").get_attribute("href")
            self.dlcs.append(DLC(name, PClink, MAClink, TORRENTlink))

    def connect(self, url):
        self.driver.get(url)
        self.collect_thread_list()
        self.collect_thread_urls()
        # self.debug_write_info_to_file()
        self.connect_to_thread()
    
    def get_dlcs(self):
        return self.dlcs

    def __init__(self, url, page_number):
        currentDirectory = os.path.dirname(__file__)

        geckoPath = os.path.join(currentDirectory, "geckodriver\\geckodriver.exe")
        profilePath = os.path.join(currentDirectory, "Tor Browser\\Browser\\TorBrowser\\Data\\Browser")
        profile = FirefoxProfile(profilePath)

        profile.set_preference('network.proxy.type', 1)
        profile.set_preference('network.proxy.socks', '127.0.0.1')
        profile.set_preference('network.proxy.socks_port', 9150)
        profile.set_preference("network.proxy.socks_remote_dns", True)
        profile.update_preferences()

        self.page_number = 0
        self.dlc_post_urls = []
        self.dlcs = []

        self.driver = webdriver.Firefox(firefox_profile=profile, executable_path=geckoPath)
        self.connect(url)
        self.page_number = page_number
        self.dlc_post_urls = []
