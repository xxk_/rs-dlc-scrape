from browser import TorBrowser
from DLCFetcher import DLCFetcher
from table_writer import TableWriter

def main():
    page_urls = []
    dlc_lists = []

    browser = TorBrowser("LINK OMITTED")
    page_urls = browser.page_urls
    browser.quit()

    for i in range(len(page_urls)):
        print("PAGE: " + str(i))
        dlc_fetcher = DLCFetcher(page_urls[i], i)
        dlc_lists.append(dlc_fetcher.get_dlcs())
        dlc_fetcher.quit()

    TableWriter(dlc_lists)

if __name__ == "__main__":
    main()