import os

from selenium import webdriver
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
from selenium.webdriver.firefox.firefox_profile import FirefoxProfile

class TorBrowser:
    
    def __init__(self, url):
        currentDirectory = os.path.dirname(__file__)

        geckoPath = os.path.join(currentDirectory, "geckodriver\\geckodriver.exe")
        profilePath = os.path.join(currentDirectory, "Tor Browser\\Browser\\TorBrowser\\Data\\Browser")
        profile = FirefoxProfile(profilePath)

        profile.set_preference('network.proxy.type', 1)
        profile.set_preference('network.proxy.socks', '127.0.0.1')
        profile.set_preference('network.proxy.socks_port', 9150)
        profile.set_preference("network.proxy.socks_remote_dns", True)
        profile.update_preferences()

        self.releasesPage = None
        self.page_urls = []
        self.dlc_posts = []

        self.driver = webdriver.Firefox(firefox_profile=profile, executable_path=geckoPath)
        self.connect(url)

    def debug_write_info_to_file(self):
        outputFile = open("output\\post_titles.txt", "w+")
        for dlcPost in self.dlc_posts:
            linkText = dlcPost.find_element_by_tag_name("a")
            outputFile.write(linkText.text + "\n")
        for pageURL in self.page_urls:
            outputFile.write(pageURL + "\n")
        outputFile.close()

    def debug_screenshot(self):
        self.driver.save_screenshot("test_ss.png")

    def collect_thread_list(self):
        threadlist = self.driver.find_element_by_class_name("threadlist")
        self.dlc_posts = threadlist.find_elements_by_tag_name("li")

    def collect_page_urls(self):
        pageList = self.driver.find_element_by_class_name("multipage")
        self.page_urls.append(self.driver.current_url)
        for multipageItem in pageList.find_elements_by_tag_name("a"):
            self.page_urls.append(multipageItem.get_attribute("href"))

    def quit(self):
        self.driver.quit()
    
    def connect(self, url):
        self.driver.get(url)
        self.collect_thread_list()
        self.collect_page_urls()
        self.debug_write_info_to_file()
   